//function is to convert string API result to number data type.
function toNum(str) {
    // convert string to arrat to gain access to our arry methods
    const arr = [...str] // usa a spread operator to acquire all string data captured from our API services
    //lets filterout the commas inside the string

    const filteredArray = arr.filter(element => element !== ",");
    return paseInt(filteredArray.reduce((x, y) => x + y ))
}

//['3,455,666','966,355']