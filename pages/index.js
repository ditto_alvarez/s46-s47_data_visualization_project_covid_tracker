import Head from 'next/head'
// import jumbotron
import Jumbotron from 'react-bootstrap/Jumbotron';

// we are going to be creating a COVID-19 tracker app.
export default function Home() {
  return (
    <>
      <Jumbotron>
        <h1> Total Covid-19 Cases in the world:  </h1>
        </Jumbotron>
      </>
   
  )
}


export async function getStaticProps() {
  //fetch ghe data from our API endpoint
 const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "Get",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
    }


 })
  // the data that will be taken from the API readable has to readable in the browser.

  const data = await res.json()

  //since the data is already converted into a json format making parsable to get its properties
  const couintriesStats = data.countries_stat //this property is provided by the API services. this property will allow us to get data which describes the total of COVID cases in all countries
  
  
}